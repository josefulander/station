<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />		
		<link href='https://fonts.googleapis.com/css?family=Vollkorn|Work+Sans:700,500,300' rel='stylesheet' type='text/css'>
		<link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/favicon.ico?v=2" type="image/x-icon">
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/apple-touch-icon-144x144-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/apple-touch-icon-114x114-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/apple-touch-icon-72x72-precomposed.png">
		<link rel="apple-touch-icon-precomposed" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/apple-touch-icon-precomposed.png">
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
	<?php do_action( 'foundationpress_after_body' ); ?>
	<div id="loader-wrapper">
		<div id="loader"></div>

		<div class="loader-section"></div>

	</div>

<section class="sidebar show-for-large-up grey-1">
	<div class="hide-scroll">
    		<div class="scroll-div">
    			<div>
    			<a href="<?php echo get_site_url(); ?>"> <img class="side-nav-logo" src="<?php echo get_stylesheet_directory_uri() ; ?>/assets/images/logo/kafe_station_logo.png"></a>
    			</div>
    				
					<?php foundationPress_side_nav(); ?>
					
					<div class="social-icons">
			
					  <a href="https://www.facebook.com/Kaf%C3%A9-Station-137668152955179/?ref=hl">
					    <i class="fa fa-facebook"></i>
					  </a>
					 
					  <a href="https://instagram.com/kafestation/">
					    <i class="fa fa-instagram"></i>
					  </a>

					</div>
					<a href="http://umea.pingst.se"><img class="pingst-logo" alt="Pingst Umeå" src="<?php echo get_stylesheet_directory_uri() ; ?>/assets/images/logo/pingst_umea_vit.png"></a>
				</div>
				
		</div>
</section>

<section class="container" role="document">
	<?php do_action( 'foundationpress_after_header' ); ?>

	<?php get_template_part( 'parts/top-bar' ); ?>