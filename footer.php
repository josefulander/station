<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

	<div id="footer-container" class="box">
		<footer id="footer" class="row">
			<?php do_action( 'foundationpress_before_footer' ); ?>
			<?php dynamic_sidebar( 'footer-widgets' ); ?>
			<?php do_action( 'foundationpress_after_footer' ); ?>
		</footer>
		<div class="small-12 large-8 columns copyright">
				<span>
					Copyright 
					<?php
					$year = date('Y');
					if ($year != 2015){
					echo '© 2015 – '.$year.'  kafestation.se';
					} else {
					echo '© 2015  kafestation.se';
					}
					?>
					&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;Theme by <a href="http://krokodilwebb.se">Krokodil Webbyrå</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;All Rights Reserved&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;Powered by <a href="http://wordpress.org">WordPress</a>
				</span>

			</div>
			<div class="small-12 large-4 columns">
				<div class="social-icons">
			
				  <a href="https://www.facebook.com/Kaf%C3%A9-Station-137668152955179/?ref=hl">
				    <i class="fa fa-facebook"></i>
				  </a>
				 
				  <a href="https://instagram.com/kafestation/">
				    <i class="fa fa-instagram"></i>
				  </a>

				</div>
			</div>
	</div>
</section>

<?php do_action( 'foundationpress_layout_end' ); ?>

<?php wp_footer(); ?>
<?php do_action( 'foundationpress_before_closing_body' ); ?>
</body>
</html>
