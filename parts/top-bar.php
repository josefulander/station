<?php
/**
 * Template part for top bar menu
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<div class="top-bar-container contain-to-grid hide-for-large-up">
    <nav class="top-bar" data-topbar role="navigation">
        <ul class="title-area top-bar-<?php echo apply_filters( 'filter_mobile_nav_position', 'mobile_nav_position' ); ?>">
            <li class="name">
                <a href="<?php echo get_site_url(); ?>"><img class="" src="<?php echo get_stylesheet_directory_uri() ; ?>/assets/images/logo/kafe_station_logo.png"></a>
                <h1><a href="<?php echo home_url(); ?>">STATION</a></h1>
            </li>
            <li class="toggle-topbar menu-icon"><a href="#"><span></span></a></li>
        </ul>
        <section class="top-bar-section">
            <?php foundationpress_top_bar_l(); ?>
            <?php foundationpress_top_bar_r(); ?>
            <div class="social-icons">

                <a href="http://umea.pingst.se"><img class="pingst-logo" alt="Pingst Umeå" src="<?php echo get_stylesheet_directory_uri() ; ?>/assets/images/logo/pingst_umea_vit.png"></a>
                <span>
                   <a href="https://www.facebook.com/Kaf%C3%A9-Station-137668152955179/?ref=hl">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a href="https://instagram.com/kafestation/">
                        <i class="fa fa-instagram"></i>
                    </a> 
                </span>

            </div>
            
        </section>
    </nav>
</div>
