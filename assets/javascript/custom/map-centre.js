$(function(){
  if ($("body").hasClass("page-template-page-contact")) {
    var myCenter=new google.maps.LatLng(63.825706, 20.263000);
    var markerPos=new google.maps.LatLng(63.824143, 20.262970);
    function initialize()
    {
    var mapProp = {
        center:myCenter,
        scrollwheel: false,
        draggable: true,
        zoom:15,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        styles: [{"stylers":[{"saturation":-100},{"gamma":1}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"labels.text","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"poi.place_of_worship","elementType":"labels.text","stylers":[{"visibility":"off"}]},{"featureType":"poi.place_of_worship","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"geometry","stylers":[{"visibility":"simplified"}]},{"featureType":"water","stylers":[{"visibility":"on"},{"saturation":50},{"gamma":0},{"hue":"#50a5d1"}]},{"featureType":"administrative.neighborhood","elementType":"labels.text.fill","stylers":[{"color":"#333333"}]},{"featureType":"road.local","elementType":"labels.text","stylers":[{"weight":0.5},{"color":"#333333"}]},{"featureType":"transit.station","elementType":"labels.icon","stylers":[{"gamma":1},{"saturation":50}]}],
              
      };

    var map=new google.maps.Map(document.getElementById("map-canvas"),mapProp);

    var image = new google.maps.MarkerImage(
            "http://localhost/station/wp-content/themes/station/assets/images/icons/map-marker.png",
            null, /* size is determined at runtime */
            null, /* origin is 0,0 */
            null, /* anchor is bottom center of the scaled image */
            new google.maps.Size(30, 41)
        );
    var marker=new google.maps.Marker({
      position:markerPos,
      icon:image
      });

    marker.setMap(map);

    var infowindow = new google.maps.InfoWindow({
      content:'<div class="info-content">' +
            '<h1>Kontakt</h1>' +
            '<p>TELEFON: 090-14 19 90</p><p>E-POST: <a href="mailto:INFO@KAFESTATION.COM">INFO@KAFESTATION.COM</a></p><p>ÖSTRA RÅDHUSGATAN 2C</p><p>903 26 UMEÅ</p>' + 
            '</div>'
      });

    google.maps.event.addListener(marker, 'click', function() {
      infowindow.open(map,marker);
    });
    infowindow.open(map,marker);

    }

    google.maps.event.addDomListener(window, 'resize', initialize);
    google.maps.event.addDomListener(window, 'load', initialize);
  }
});