$(document).ready(function() {
  function setHeight() {
    windowHeight = $(window).innerHeight();
    //$('.sidebar').css('height', windowHeight);
    $('.hide-scroll').css('height', windowHeight);
  };
  setHeight();
  
  $(window).resize(function() {
    setHeight();
  });
});


//Checks if scrollbar is visible
(function($) {
    $.fn.hasScrollBar = function() {
        return this.get(0).scrollHeight > this.height();
    }
})(jQuery);

//Fix scroll bar margin
$(function sideNavScroll() {

	// Create the measurement node
	var scrollDiv = document.createElement("div");
	scrollDiv.className = "scrollbar-measure";
	document.body.appendChild(scrollDiv);

	// Get the scrollbar width
	var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;
	//console.warn(scrollbarWidth); // Mac:  15

	// Delete the DIV 
	document.body.removeChild(scrollDiv);

	var scrollDivMargin = -300;
    var menuMargin = scrollDivMargin - scrollbarWidth;
    
	if( $('.scroll-div').hasScrollBar() ) {
		$('.scroll-div').css('margin-right', menuMargin);
	} 
	else {
		$('.scroll-div').css('margin-right','-300px');
	}

	$(window).resize(function() {
	    if( $('.scroll-div').hasScrollBar() ) {
			$('.scroll-div').css('margin-right', menuMargin);
		} 
		else {
			$('.scroll-div').css('margin-right','-300px');
		}
	});

});
