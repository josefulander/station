$(window).scroll(function () {
    var scrollTop = $(window).scrollTop();
    var height = $(window).height();
    $('#parallax1').css({
        'opacity': ((height - scrollTop) / height)
    });
});

$(document).ready(function(){
	//Adds the class loaded to the body element
    $('body').addClass('loaded');
    //Fungrion that detects when transition is finished on element
    $("#loader-wrapper").one('transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd', 
	function() {
		//Hide loading screen wrapper
		$('#loader-wrapper').css({'display': 'none' });
	});
});


