//Full height
function setFullHeight() {
    var windowFullHeight = $(window).innerHeight();
    $('.full-height').css('height', windowFullHeight);
};

function setSectionHeight() {	
    var contentHeight = $('.content-heading').height()*1.5;
    $('.hero.coffe').css({'height': contentHeight+'px' });
};

function setSectionHeight2() {	
    var contentHeight2 = $('.content-heading2').height()*1.5;
    $('.hero.coffe').css({'height': contentHeight2+'px' });
};

function setSectionHeight3() {	
    var contentHeight3 = $('.content-heading3').height()*1.5;
    $('.hero.coffe').css({'height': contentHeight3+'px' });
};


// Calculate conten width and offset content to center
function centerContent() {	
    var containerWidth = $('.hero.coffe').width();
    var contentWidth = containerWidth*0.9;
    var contentDiff = containerWidth - contentWidth;
    var contentMove = contentDiff / 2;
    $('.hero.coffe .content-heading,.hero.coffe .content-heading2, .hero.coffe .content-heading3').css({'left': contentMove+'px' });
};

function addActiveClass() {
	if ($(".single-tribe_events")[0] || $(".events-archive")[0]){
    	$( ".menu-item-37" ).addClass( "active" );
	} 
};

$( document ).ready(function() {
	addActiveClass();
	setTimeout(function() {
      setFullHeight();
    }, 1);
    setSectionHeight();
    if ($(".page-template-page-menu")[0]){
    	setSectionHeight2();
    	setSectionHeight3();
	} 
    centerContent();
});

$(window).resize(function() {
    setTimeout(function() {
      setFullHeight();
    }, 1);
    setSectionHeight();
    if ($(".page-template-page-menu")[0]){
    	setSectionHeight2();
    	setSectionHeight3();
	}
    centerContent();
});