<?php
/*
Template Name: Menysida
*/
get_header(); ?>

<div class="hero menu">
  	<div class="content">
	  	<div id="parallax1" class="parallaxParent">
			<div class="parallax-bg">
				<div class="content-heading"><!--This needs to be here for the vertical alignment to work-->
					<div class="wow fadeInUp" data-wow-duration="1.8s" data-wow-delay="0s">
				  		<h1 class="large-h1">Lunchmeny & fika</h1>
				  		<div class="divider divider-center"></div>
				  		<h2>Sallader, pastarätter, mackor, morotskaka, chokladbollar och kaffe.</h2>
			  		</div>
			  	</div>
			</div>
		</div>
  	</div>
</div>


<div class="row" data-equalizer>
	
	<div class="small-12 large-8 columns box grey-4" data-equalizer-watch>		
		<?php /* Start loop */ ?>
		<?php while ( have_posts() ) : the_post(); ?>
		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<header>
				<h2 class="large-h2 entry-title"><?php echo "Lunchmeny vecka " . ltrim(date('W'),0) ?></h2>
				<div class="divider"></div>
			</header>
			<div class="entry-content">
				<?php the_content(); ?>
			</div>
		</article>
		<?php endwhile; // End the loop ?>
	</div>

	<div class="small-12 large-4 columns nopadding buffe" data-equalizer-watch>
		<div class="box grey-3 ">
			<h2 class="subheader">I lunchen ingår</h2>
			<h4>Salladsbuffé, hembakat matbröd, måltidsdryck, kaffe/te och kaka.</h4>
		</div>
	</div>

</div>

<div class="row nomargin">

	<div class="small-12 columns">
		<div class="box grey-2">
			<h2 class="subheader">Dagligt lunchalternativ</h2>
			<h2>Varje dag serveras även dagens soppa</h2>
		</div>
	</div>

</div>

<div class="row nomargin features" data-equalizer>

	<div class="small-12 medium-6 large-4 columns">
		<img src="<?php echo get_stylesheet_directory_uri() ; ?>/assets/images/backgrounds/avokadosallad.jpg" alt="avokadosallad">
		<div class="box grey-3" data-equalizer-watch>
			<h2 class="subheader">Dagligt lunchalternativ</h2>
			<h4>Pasta eller quinoasallad</h4>
			<ul>
				<li>Ceasarsallad</li>
				<li>Avokado med keso och skagenröra</li>
				<li>Laxsallad med keso och skagenröra</li>
				<li>Räkor</li>
				<li>Grekisk sallad</li>
			</ul>
		</div>
	</div>

	<div class="small-12 medium-6 large-4 columns">
		<img src="<?php echo get_stylesheet_directory_uri() ; ?>/assets/images/backgrounds/laxtallrik.jpg" alt="laxtallrik">
		<div class="box grey-2 " data-equalizer-watch>
			<h2 class="subheader">Dagligt lunchalternativ</h2>
			<h4>Hälsotallrik</h4>
			<p>Med bl.a quinoa, hummus, keso, bönor, morötter</p>
			<ul>
				<li>Kallrökt lax</li>
				<li>Kyckling</li>
				<li>Avokado</li>
				<li>Räkor</li>
				<li>Fetaost</li>
			</ul>
		</div>
	</div>

	<div class="small-12 medium-6 large-4 columns">
		<img src="<?php echo get_stylesheet_directory_uri() ; ?>/assets/images/backgrounds/pie_small.jpg" alt="paj med sallad">
		<div class="box grey-4" data-equalizer-watch>
			<h2 class="subheader">Dagligt lunchalternativ</h2>
			<h4>Hemlagade pajer</h4>
			<ul>
				<li>Köttfärs</li>
				<li>Skinka och broccoli</li>
				<li>Grekisk paj</li>
				<li>Västerbottenost med husets skagenröra</li>
			</ul>
		</div>
	</div>

</div>

<div class="hero coffe">
	<div class="content">

  		<div id="parallax2" class="parallaxParent">
			<div class="pancake-section"></div>
		</div>

		<div class="content-heading2"><!--This needs to be here for the vertical alignment to work-->
			<div class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0s">
				<h2>Alltid på Kafé Station</h2>
				<div class="divider divider-center"></div>
		  		<h2 class="large-h2">Pannkaka med hemlagad sylt och grädde till barnen</h2>
	  		</div>
	  	</div>

  </div>
</div>

<div class="row nomargin" data-equalizer>

	<div class="small-12 medium-4 columns">
		<div class="box grey-2" data-equalizer-watch>
			<h2 class="subheader">Alltid i kafét</h2>
			<h2>Mackor</h2>
			<div class="divider"></div>
			<ul>
				<li>Ljus ciabatta med  skagenröra</li>
				<li>Mörk  ciabatta med kycklingröra</li>
				<li>Baguette: med ost och skinka, brieost och salami eller kikärtsröra</li>
				<li>Surdegsbröd med chévreost</li>
				<li>Surdegsbröd med grekisk röra och pesto</li>
				<li>Tunnbrödrulle med vindelrökt skinka</li>
				<li>Fullkorn med lax och philadelfiaost</li>
				<li>Grovbulle med ost</li>
			</ul>
		</div>
	</div>

	<div class="small-12 medium-4 columns">
		<div class="box grey-1 " data-equalizer-watch>
			<h2 class="subheader">Alltid i kafét</h2>
			<h2>Hemlagade matpajer</h2>
			<div class="divider"></div>
			<p>Serveras hela dagen, inkl måltidsdryck, sallad och kaffe/te:</p>
			<ul>
				<li>Köttfärs</li>
				<li>Skinka och broccoli</li>
				<li>Fetaost och oliver</li>
				<li>Västerbottenost med husets skagenröra</li>
				<li>Grönkål med Chevréost</li>
			</ul>
		</div>
	</div>

	<div class="small-12 medium-4 columns">
		<div class="box grey-3 " data-equalizer-watch>
			<h2 class="subheader">Alltid i kafét</h2>
			<h2>Tacotallrik</h2>
			<div class="divider"></div>
			<p>Tacotallrik med klassiska tillbehör:</p>
			<ul>
				<li>Salsa</li>
				<li>Gräddfil</li>
				<li>Grönsaker</li>
				<li>Nachos</li>
			</ul>
			<p>Finns att få med tacokryddad köttfärs eller qournfärs.</p>
		</div>
	</div>

</div>

<div class="hero coffe">
	<div class="content">

  		<div id="parallax3" class="parallaxParent">
			<div class="pastery-section"></div>
		</div>

		<div class="content-heading3"><!--This needs to be here for the vertical alignment to work-->
			<div class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0s">
				<h2>Alltid på Kafé Station</h2>
				<div class="divider divider-center"></div>
		  		<h2 class="large-h2">Naturligtvis har vi massor med gott fika</h2>
	  		</div>
	  	</div>

  </div>
</div>

<div class="row nomargin features" data-equalizer>

	<div class="small-12 medium-6 large-3 columns grey-2" data-equalizer-watch>
		
		<div class="box">	
			<h2 class="subheader">Fikabröd</h2>
				<h3>Morotskaka</h3>
		</div>
		<img src="<?php echo get_stylesheet_directory_uri() ; ?>/assets/images/backgrounds/morotskaka.jpg" alt="morotskaka">
  </div>

  <div class="small-12 medium-6 large-3 columns grey-4" data-equalizer-watch>
  	
		<div class="box">	
	  	<h2 class="subheader">Efterrätt</h2>
	  	<h3>Kladdkaka</h3>
  	</div>
  	<img src="<?php echo get_stylesheet_directory_uri() ; ?>/assets/images/backgrounds/kladdkaka.jpg" alt="kladdkaka">
  </div>

  <div class="small-12 medium-6 large-3 columns grey-2" data-equalizer-watch>
  	
  	<div class="box">
	  	<h2 class="subheader">Cheesecake</h2>
			<h3>Jordgubbskaka</h3>
		</div>
		<img src="<?php echo get_stylesheet_directory_uri() ; ?>/assets/images/backgrounds/jordgubbscheesecake.jpg" alt="cheesecake jordgubb">
  </div>

  <div class="small-12 medium-6 large-3 columns grey-4" data-equalizer-watch>
  	
  	<div class="box">
			<h2 class="subheader">Cheesecake</h2>
			<h3>Chokladkaka</h3>
		</div>
		<img src="<?php echo get_stylesheet_directory_uri() ; ?>/assets/images/backgrounds/cheescake-choklad.jpg" alt="cheesecake choklad">
  </div>

</div>

<?php get_footer(); ?>
