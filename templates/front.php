<?php
/*
Template Name: Front
*/
get_header(); ?>

<div id="parallax1" class="parallaxParent hero start full-height">
  <div class="parallax-front">
    <div class="parallax-content">
    	<div class="content-wrap">
    		<div class="circle-wrap wow fadeInUp" data-wow-duration="2s" data-wow-delay="0s">
    			<h1>
			      VÄLKOMNA<br>
			      TILL STATION!<br>
			      <span>
				      KAFÉ OCH LUNCH-<br>
				      RESTAURANG<br>
			      </span>
			      I CENTRALA<br>
			      UMEÅ
			      </h1>
    		</div>
      </div>
    </div>
  </div>
</div>

<div class="row">

	<div class="small-12 columns alert-yellow" data-equalizer-watch>
		<div class="box-tiny text-center">	
  			<h2 class="">Nu söker vi ny verksamhetschef!  </h2>
  			<a href="<?php echo site_url(); ?>/jobb" class="button alert">LÄS MER >></a>
		</div>
  	</div>

</div>


<div class="row" data-equalizer data-options="equalize_on_stack: true">

	<div class="small-12 large-8 columns box grey-4" data-equalizer-watch>
		<h2 class="large-h2">Kafé Station - med stans bästa läge</h2>
		<h2 class="subheader">Mitt i staden mellan broarna</h2>
		<div class="divider"></div>
		<p>
			Välkommen att slå dig ner hos oss en stund. Vi bjuder på vällagad mat och gott kaffe. På menyn hittar du bla. pajer, sallader, soppa och ett gott fikautbud. Passa även på att prova på vår populära morotskaka!
		</p>
		<p>Tack till alla er som kommer hit till Kafé Station varje dag. Vi är glada att ni uppskattar det vi gör och vi lovar att göra det bästa för att du ska fortsätta trivas hos oss!</p>
  </div>

  <div class="small-12 large-4 columns map" data-equalizer-watch>
  </div>

</div>

<div class="row nomargin" data-equalizer>

	<div class="small-12 large-4 columns grey-2" data-equalizer-watch>
		<div class="facebook">	
			<?php echo do_shortcode('[custom-facebook-feed num=1]'); ?>
		</div>
  	</div>

  	<div class="small-12 large-4 columns grey-4" data-equalizer-watch>
  		<h3 class="instagram-header"><i class="fa fa-instagram"></i>Instagram</h3>
		<div class="instagram">
			<div class="box">
				<?php echo do_shortcode('[instagram-feed num=1 cols=1]'); ?>
			</div>
		</div>
 	 </div>

 	 <div class="small-12 large-4 columns grey-3" data-equalizer-watch>
  		<h3 class="calendar-header"><i class="fa fa-calendar"></i>Evenemang</h3>
		<div class="front-page-events">
			<div class="box">
				<?php if ( is_active_sidebar( 'front_page_calendar' ) ) : ?>
					<?php dynamic_sidebar( 'front_page_calendar' ); ?>
				<?php endif; ?>
			</div>
		</div>
 	 </div>

</div>

<div class="hero coffe">
	<div class="content">

  	<div id="parallax2" class="parallaxParent">
			<div class="coffe-section"></div>
		</div>

		<div class="content-heading"><!--This needs to be here for the vertical alignment to work-->
			<div class="wow fadeInUp" data-wow-duration="1.8s" data-wow-delay="0s">
	  		<h2 class="large-h2">Vi serverar gott kaffe!</h2>
	  		<div class="divider divider-center"></div>
	  		<h2>Naturligtvis är vårt kaffe kravmärkt och ekologiskt odlat</h2>
  		</div>
  	</div>

  </div>
</div>


<div class="row nomargin features" data-equalizer>

	<div class="small-12 medium-6 columns grey-2" data-equalizer-watch>
		<div class="box">	
			<h2 class="subheader">Events</h2>
  			<h3>Vi har perfekta lokaler för konferenser, seminarier eller musikevents.</h3>
		</div>
  	</div>

  <div class="small-12 medium-6 columns grey-3" data-equalizer-watch>
		<div class="box">	
	  	<h2 class="subheader">Öppettider</h2>
	  	<?php /* Start loop */ ?>
		<?php while ( have_posts() ) : the_post(); ?>
		
				<?php the_content(); ?>
			
		<?php endwhile; // End the loop ?>
  	</div>
  </div>

 </div>

<div class="row nomargin features" data-equalizer>

	<div class="small-12 medium-6 large-3 columns grey-2" data-equalizer-watch>
		<img src="<?php echo get_stylesheet_directory_uri() ; ?>/assets/images/backgrounds/food.jpg">
		<div class="box">	
			<h2 class="subheader">Lunchrestaurang</h2>
  			<h3>Goda luncher</h3>
		</div>
  </div>

  <div class="small-12 medium-6 large-3 columns grey-4" data-equalizer-watch>
  	<img src="<?php echo get_stylesheet_directory_uri() ; ?>/assets/images/backgrounds/fika-kakor.jpg">
		<div class="box">	
	  	<h2 class="subheader">Kafé</h2>
	  	<h3>Kaffe och kakor</h3>
  	</div>
  </div>

  <div class="small-12 medium-6 large-3 columns grey-2" data-equalizer-watch>
  	<img src="<?php echo get_stylesheet_directory_uri() ; ?>/assets/images/backgrounds/setting.jpg">
  	<div class="box">
	  	<h2 class="subheader">Stämning</h2>
			<h3>Unik och rustik miljö</h3>
		</div>
  </div>

  <div class="small-12 medium-6 large-3 columns grey-4" data-equalizer-watch>
  	<img src="<?php echo get_stylesheet_directory_uri() ; ?>/assets/images/backgrounds/wifi.jpg">
  	<div class="box">
			<h2 class="subheader">Internet</h2>
			<h3>Gratis Wi-Fi</h3>
		</div>
  </div>

</div>


<?php get_footer(); ?>