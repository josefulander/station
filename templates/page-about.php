<?php
/*
Template Name: About
*/
get_header(); ?>

<div class="hero about">
  	<div class="content">
	  	<div id="parallax1" class="parallaxParent">
			<div class="parallax-bg">
				<div class="content-heading"><!--This needs to be here for the vertical alignment to work-->
					<div class="wow fadeInUp" data-wow-duration="1.8s" data-wow-delay="0s">
				  		<h1 class="large-h1">Om Kafé Station</h1>
				  		<div class="divider divider-center"></div>
				  		<h2>Historik och verksamhetsidé</h2>
			  		</div>
			  	</div>
			</div>
		</div>
  	</div>
</div>

<div class="row nomargin" data-equalizer>

	<div class="small-12 medium-6 columns windows" data-equalizer-watch>
	</div>

	<div class="small-12 medium-6 columns grey-3" data-equalizer-watch>
	<div class="box">	
  		<h2 class="subheader">Historik</h2>
			<h2>Ett anrikt tornhus</h2>
			<div class="divider"></div>
			<p>Fastigheten uppfördes 1892 för att som stadens första elverk betjäna 36 gatlyktor, kyrkan och rådhuset med el. Här rymdes även brandkåren som torkade slangar i tornet. Under årens lopp har den mycket anrika fastigheten genomgått flera omfattande förändringar och huserat en mängd olika verksamheter för att 1996 förvärvas av Pingstkyrkan i Umeå. Året därpå startade Pingstkyrkan även verksamheten Stationen i Umeå AB, i folkmun Kafé Station, som idag är ett välkänt och populärt kafé.</p>
  	</div>
	</div>

</div>

<div class="row nomargin" data-equalizer>

	<div class="small-12 medium-6 columns grey-4" data-equalizer-watch>
	<div class="box">	
  		<h2 class="subheader">Verksamhetsidé</h2>
			<h2>Ett drogfritt alternativ</h2>
			<div class="divider"></div>
			<p>Kafé Station är ett drogfritt alternativ och med ett attraktivt kaféutbud i trivsam miljö vill vi förmedla glädjen i det kristna budskapet. I våra lokaler anordnas därför spännande Alpha-kurser, föreläsningar och konserter såväl som mer riktade aktiviteter till tonåringar och unga vuxna som ligger i linje med vår värdegrund. Kafé Station vill med sina 18 anställda och många ideella hälsa dig varmt välkommen. Vi tror på en kraft som är äldre än 1892 och som kan ge ljus till alla!</p>
  	</div>
	</div>

	<div class="small-12 medium-6 columns restaurant-2" data-equalizer-watch>
	</div>

 </div>
<?php get_footer(); ?>
