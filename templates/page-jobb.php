<?php
/*
Template Name: Jobb
*/
get_header(); ?>

<div class="hero jobb">
  	<div class="content">
	  	<div id="parallax1" class="parallaxParent">
			<div class="parallax-bg">
				<div class="content-heading"><!--This needs to be here for the vertical alignment to work-->
					<div class="wow fadeInUp" data-wow-duration="1.8s" data-wow-delay="0s">
				  		<h1 class="large-h1">Kafé Station söker ny Verksamhetschef!</h1>
				  		<div class="divider divider-center"></div>
				  		<h2>Har Du det som krävs?</h2>
			  		</div>
			  	</div>
			</div>
		</div>
  	</div>
</div>

<div class="row nomargin" data-equalizer>

	<div class="small-12 medium-6 columns coffe-2" data-equalizer-watch>
	</div>

	<div class="small-12 medium-6 columns grey-3" data-equalizer-watch>
	<div class="box">	
  		<h2 class="subheader">Bakgrund</h2>
			<h2>Rollbeskrivning</h2>
			<div class="divider"></div>
			<p>Vår tidigare verksamhetschef Ann-Sofi Lund flyttade till Stockholm i mars 2017 efter att ha arbetat på Kafé Station i ett femtontal år och som Du förstår lämnade hon ett stort tomrum efter sig. Styrelsen valde att under våren arbeta projektorienterat med en tillförordnad och tidsbegränsad lösning och har nu den stora glädjen att söka Dig som vill kliva in som permanent Verksamhetschef.</p>
			<p>Vi ger Dig operativt huvudansvar för driften av Kafé Station och tror att Du är ett stabilt bollplank för personalen när det gäller drift, underhåll, rutiner och utvecklingsfrågor samt också beredd att täcka upp om så kvävs. Vidare kommunicerar Du aktivt med marknaden, Stations kunder och övriga intressenter för att utveckla verksamheten och bibehålla engagemanget hos medarbetarna. Du upprättar och ser till att verksamhetsplan för Station innehåller kvalitativa aktiviteter för målgrupperna och har en god dialog med personal och styrelse.
			</p>
  	</div>
	</div>

</div>

<div class="row nomargin" data-equalizer>

	<div class="small-12 medium-6 columns grey-4" data-equalizer-watch>
	<div class="box">	
  		<h2 class="subheader">Verksamhet</h2>
			<h2>EXEMPEL PÅ ARBETSUPPGIFTER OCH ANSVARSOMRÅDEN</h2>
			<div class="divider"></div>
			<ul>
				<li>Personalledning och delegering av arbetsuppgifter</li>
				<li>Ekonomistyrning och fakturering för att säkerställa lönsamhet</li>
				<li>Rekrytering av tillsvidare, vikariat, ALU, ideella mfl</li>
				<li>Löpande personalutveckling</li>
				<li>Ansvarig gentemot Miljökontoret och Arbetsmiljöverket</li>
				<li>Avtalsförhandling med leverantörer</li>
				<li>Inköp av inventarier, underhållsmaterial</li>
				<li>Planering och koordination av ideella insatser</li>
				<li>Utveckling av marknadsföringsstrategi och drivande i marknadsföringsfrågor</li>
				<li>Ansvarig för försäljning och bokning gentemot kunder gällande uthyrning mm</li>
				<li>Kommunicerar löpande med intressenter hos Umeå Pingst (ägare av verksamheten) och koordinerar på så sätt att verksamhetsplanen linjerar med ägarens målsättning</li>
			</ul>
  	</div>
	</div>

	<div class="small-12 medium-6 columns planning" data-equalizer-watch>
	</div>

 </div>

 <div class="row nomargin" data-equalizer>

	<div class="small-12 medium-6 columns organizing" data-equalizer-watch>
	</div>

	<div class="small-12 medium-6 columns grey-3" data-equalizer-watch>
	<div class="box">	
  		<h2 class="subheader">Din bakgrund</h2>
			<h2>Kvalifikationer</h2>
			<div class="divider"></div>
			<p>Vi tror att Du har högskoleutbildning (eller motsvarande) och mångårig erfarenhet av ledarskap med personalansvar. Du har också haft ansvar för budget, försäljning och resultat. Det är förstås meriterande om Du tidigare arbetat i kafé-, restaurang- och konferensverksamhet men är på inget sätt en nödvändighet.
			</p>
  	</div>
	</div>

</div>

<div class="row nomargin" data-equalizer>

	<div class="small-12 medium-6 columns grey-4" data-equalizer-watch>
	<div class="box">	
  		<h2 class="subheader">Dina egenskaper</h2>
			<h2>Vi söker dig som...</h2>
			<div class="divider"></div>
			<p>...arbetar med stort engagemang och hög effektivitet. Som person kommunicerar Du klart och tydligt med Din omgivning och Du har ett sätt som entusiasmerar och uppmuntrar Dina medarbetare.
			I Ditt möte och kommunikation med kunder och gäster är Du positiv och ger ett varmt bemötande för att tillgodose behov och önskemål.</p>
			<p>Vidare är Du strukturerad och har en självklar förmåga att arbeta med koordination och struktur. Du sympatiserar med Kafé Stations värdegrund och vi tror att Du har en prestigelöshet och ödmjukhet som matchar din mångåriga erfarenhet och seniora tyngd.</p>
			<p>Du gillar entreprenörskap samtidigt som Du anser att livet är mer, större, och viktigare, än ett arbete. Detta kan vara en av anledningarna till att en tjänst på cirka 50% passar Dig. Kanske har Du andra engagemang som Du vill fortsätta driva? Kanske söker Du möjlighet att prioritera tid med familj, nära och kära? </p>
  	</div>
	</div>

	<div class="small-12 medium-6 columns checklist" data-equalizer-watch>
	</div>

 </div>

 <div class="row nomargin" data-equalizer>

	<div class="small-12 medium-6 columns windows" data-equalizer-watch>
	</div>

	<div class="small-12 medium-6 columns grey-3" data-equalizer-watch>
	<div class="box">	
  		<h2 class="subheader">VI GER DIG</h2>
			<h2>Vi erbjuder...</h2>
			<div class="divider"></div>
			<p>...en fri roll och stora möjligheter att vidareutveckla kafé-, restaurang-, konsert- och konferensverksamheten. Till Din hjälp har Du Kafé-chef, Kökschef, engagerade medarbetare och ideella, samt en aktiv styrelse.
			</p>
  	</div>
	</div>

</div>

<div class="row nomargin" data-equalizer>

	<div class="small-12 medium-6 columns grey-4" data-equalizer-watch>
	<div class="box">	
  		<h2 class="subheader">Värdegrund</h2>
			<h2>OM KAFÉ STATION</h2>
			<div class="divider"></div>
			<p>Kafé Station är ett drogfritt alternativ och med ett attraktivt kaféutbud i trivsam miljö vill vi förmedla glädjen i det kristna budskapet. I våra lokaler anordnas därför spännande Alpha-kurser, föreläsningar och konserter såväl som mer riktade aktiviteter till tonåringar och unga vuxna som ligger i linje med Umeå pingst värdegrund.</p> 
			<p>Vi servar också externa gäster, styrelser och företag såsom exempelvis Styrelseakademin och Umeå Universitets Vetenskapsluncher. Kafé Station vill med sina 18 anställda och många ideella hälsa dig varmt välkommen. Vi tror på en kraft som är äldre än 1892 och som kan ge ljus till alla!
			</p>
  	</div>
	</div>

	<div class="small-12 medium-6 columns restaurant-2" data-equalizer-watch>
	</div>

 </div>

  <div class="row nomargin" data-equalizer>

	<div class="small-12 medium-6 columns contact" data-equalizer-watch>
	</div>

	<div class="small-12 medium-6 columns grey-2" data-equalizer-watch>
	<div class="box">	
  		<h2 class="subheader">Kontakt</h2>
			<h2>Anställning och ansökan</h2>
			<div class="divider"></div>
			<ul>
				<li>Tänkt omfattning: 50%</li>
				<li>Start: 1 Augusti eller enligt överenskommelse</li>
				<li>Lön: Individuell lönesättning</li>
				<li>Kafé Station har kollektivavtal.</li>
			</ul>
			<p>Vill Du veta mer om tjänsten?
			Eller vill Du skicka in en ansökan med en beskrivning varför just Du är lämplig att axla rollen?</p>

			<h5>Kontakta oss:</h5>
			<p><b>André Öberg</b><br>
			Styrelseordförande<br>
			<a href="mailto:andre.kafestation@gmail.com">andre.kafestation@gmail.com</a><br>
			070-3401080<br>
			</p>
			<p><b>Clarence Jacobson</b><br>
			Tf Verksamhetschef<br>
			<a href="mailto:clarence.kafestation@gmail.com">clarence.kafestation@gmail.com</a><br>
			070-6520688<br>
			</p>
  	</div>
	</div>

</div>
<?php get_footer(); ?>
