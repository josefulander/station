<?php
/*
Template Name: Kalendersida
*/
get_header(); ?>

<div class="hero calendar">
  	<div class="content">
	  	<div id="parallax1" class="parallaxParent">
			<div class="parallax-bg">
				<div class="content-heading"><!--This needs to be here for the vertical alignment to work-->
					<div class="wow fadeInUp" data-wow-duration="1.8s" data-wow-delay="0s">
				  		<h1 class="large-h1">Vi är mer än ett Kafé</h1>
				  		<div class="divider divider-center"></div>
				  		<h2>Boka lokaler med eller utan servering</h2>
			  		</div>
			  	</div>
			</div>
		</div>
  	</div>
</div>

<div class="row nomargin hide-on-single" data-equalizer>

	<div class="small-12 large-6 columns">
		<div class="box grey-2" data-equalizer-watch>
			<h2 class="subheader">Service</h2>
			<h2>Full service för era aktiviteter</h2>
			<div class="divider"></div>
			<p>Station är ett kafé på kvällar och helger men också en plats för konserter och andra ungdomsaktiviteter. Vi erbjuder också fin miljö för konferenser.</p>
			
		</div>
	</div>

	<div class="small-12 large-6 columns">
		<div class="box grey-1 " data-equalizer-watch>
			<h2 class="subheader">Evenemang</h2>
			<h2>Boka våra lokaler för:</h2>
			<div class="divider"></div>
			<ul>
				<li>Konserter</li>
				<li>Konferenser</li>
				<li>Seminarier</li>
			</ul>
			<a href="<?php echo get_site_url(); ?>/kontakt" target="_top"><div class="button">Kontakta oss</div></a>
		</div>
	</div>

</div>

<div class="row nomargin hide-on-single">

	<div class="small-12 columns">
		<div class="box-small grey-3">
			<h2 class="large-h2 text-center">Kommande evenemang</h2>
		<h2 class="subheader text-center">Mitt i centrala Umeå</h2>
		</div>
	</div>

</div>

<?php while ( have_posts() ) : the_post(); ?>
		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<header>
				<h1 class="entry-title"><?php the_title(); ?></h1>
			</header>
			<?php do_action( 'foundationpress_page_before_entry_content' ); ?>
			<div class="entry-content">
				<?php the_content(); ?>
			</div>
			<footer>
				<?php wp_link_pages( array('before' => '<nav id="page-nav"><p>' . __( 'Pages:', 'foundationpress' ), 'after' => '</p></nav>' ) ); ?>
				<p><?php the_tags(); ?></p>
			</footer>
			<?php do_action( 'foundationpress_page_before_comments' ); ?>
			<?php comments_template(); ?>
			<?php do_action( 'foundationpress_page_after_comments' ); ?>
		</article>
	<?php endwhile;?>


<?php get_footer(); ?>